<?php

namespace Drupal\inline_image_style\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\image\Entity\ImageStyle;

/**
 * @Filter(
 *   id = "inline_image_style_filter",
 *   title = @Translation("Inline image style"),
 *   description = @Translation("Add image styles for inline images."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   weight = 20
 * )
 */
class InlineImageStyleFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    if (str_contains($text, 'data-entity-type="file"')) {
      $dom = Html::load($text);
      $xpath = new \DOMXPath($dom);
      $image_style = ImageStyle::load('inline');
      $default_scheme = \Drupal::config('system.file')->get('default_scheme');
      $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager');
      $default_stream_wrapper = $stream_wrapper_manager->getViaScheme($default_scheme);
      $default_scheme_path = base_path() . $default_stream_wrapper->getDirectoryPath() . '/';

      foreach ($xpath->query('//img[@data-entity-type="file" and @data-entity-uuid]') as $node) {
        $image_src = $node->getAttribute('src');

        if (str_starts_with($image_src, $default_scheme_path)) {
          $image_uri = $default_scheme . '://' . substr($image_src, strlen($default_scheme_path));
          if ($image_style->supportsUri($image_uri)) {
            $image_src = $image_style->buildUrl($image_uri);
            $node->setAttribute('src', $image_src);
          }
        }
      }

      $text = Html::serialize($dom);
    }

    return new FilterProcessResult($text);
  }

}
